---
layout: page
title: Paper Readings
permalink: /papers/
order: 3
---

The papers below will be analyzed in class. For any given paper, one team will present the work and highlight positive aspects (15min), while another team will discuss limitations and/or shortcomings of the approach (10mins). We will then have 5-10min for questions or additional discussion.


- **Wed. 09/11**<br>
    1. [RetroDepth](https://www.microsoft.com/en-us/research/project/retrodepth-3d-silhouette-sensing-for-high-precision-input-on-and-above-physical-surfaces/)<br>
        Main presentation: Jevin Jiang, Adriel Sumathipala <br>
        Critique: Garima Singh, Simon Mendelsohn <br>

    2. [OmniTouch](http://www.chrisharrison.net/projects/omnitouch/omnitouch.pdf)<br>
        Main presentation: Qinying Sun, Jun Kwak <br>
        Critique: Nicholas Georgiou, Emmanuel Adeniran <br>

- **Wed. 09/18**

    3. [AnglePose](http://ftp.dcs.glasgow.ac.uk/~rod/publications/RogWilSteMur11.pdf)<br>
        Main presentation: Emmanuel Adeniran, Debasmita Ghose <br>
        Critique: Nathan Tsoi, Jay Son <br>

    4. [Detecting Moving Objects using a Single Camera
on a Mobile Robot in an Outdoor Environment](http://robotics.usc.edu/publications/media/uploads/pubs/372.pdf)<br>
        Main presentation: Sally Ma <br>
        Critique: Joe Connolly, Annie Gao <br>

- **Mon. 09/30**

    5. [YOLO](https://pjreddie.com/media/files/papers/yolo.pdf)<br>
        Main presentation: Nathan Tsoi, Sydney Thompson<br>
        Critique: Sally Ma, Annie Gao<br>
        
- **Wed. 10/02**

    6. [Multi-Person 2D Pose Estimation](https://arxiv.org/abs/1611.08050)<br>
        Main presentation: Vanessa Yan <br>
        Critique: Debasmita Ghose, Jun Kwak<br>
        
- **Wed. 10/14**
        
	7. [Attention is All You Need](https://arxiv.org/abs/1706.03762)<br>
        Main presentation: Garima Singh <br>
        Critique: Debasmita Ghose <br> <br/>
   
- **Wed. 10/23**

    8. [Keyframe-based Learning from Demonstration](https://www.cc.gatech.edu/social-machines/papers/akgun12-soro.pdf)<br>
        Main presentation: William Hu, Sean Hackett <br>
        Critique: Adriel Sumathipala, Lukas Burger <br>
        
	9. [Agile Autonomous Driving](https://www.cc.gatech.edu/~bboots3/files/Deep_Imitation_RSS18.pdf)<br>
        Main presentation: Nicholas Georgiou, Debasmita Ghose <br>
        Critique: Yichao Cheng, Valerie Chen <br>
        
<!--    [On Offline Evaluation of Vision-based Driving Models](http://vladlen.info/papers/driving-evaluation.pdf)<br>
        Main presentation: TBD <br>
        Critique: TBD <br>-->
    
<!--- **Mon. 10/29**


    10. [Neural Task Programming](https://arxiv.org/pdf/1710.01813.pdf)<br>
        Main presentation: TBD <br>
        Critique: TBD <br>-->

- **Mon. 10/28.**

    10. [Inference Machines](http://www.cs.cmu.edu/~arunvenk/papers/2016/Venkatraman_ijcai_16.pdf)<br>
        Main presentation: Simon Mendelsohn, Vanessa Yan <br>
        Critique: Nicholas Georgiou, Sydney Thompson <br>

    11. [Scheduled Sampling](https://arxiv.org/abs/1506.03099)<br>
        Main presentation: Marynel <br>
        Critique: Sally Ma 

- **Mon. 11/04**

    12. [DQN](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf)<br>
        Main presentation: Valerie Chen, Annie Gao<br>
        Critique: Nathan Tsoi, Sydney Thompson<br>
    
    13. [Mastering the Game of Go](https://storage.googleapis.com/deepmind-media/alphago/AlphaGoNaturePaper.pdf)<br>
        Main presentation: Nicholas Georgiou, Emmanuel Adeniran <br>
        Critique: Jevin Jiang, William Hu <br>

- **Wed. 11/06**

    14. [How to Shape the Humor of a Robot - Social Behavior Adaptation Based on Reinforcement Learning](https://dl.acm.org/citation.cfm?id=3242976)<br>
        Main presentation: Kayleigh Bishop, Joe Connolly<br>
        Critique: Joseph Valdez, Qinying Sun<br>

    15. [Trust Region Policy Optimization](https://arxiv.org/pdf/1502.05477.pdf)<br>
        Main presentation: Cove Geary, Jay Son<br>
        Critique: Tony Fu, Yichao Cheng<br>
    
- **Wed. 11/13**

    16. [Probabilistic Pointing Target Prediction](http://www.cs.cmu.edu/~./bziebart/publications/pointing-target-prediction.pdf)<br>
        Main presentation: Yichao Cheng, Tony Fu<br>
        Critique: Marynel <br>

    17. [Generative Adversarial Imitation Learning](https://cs.stanford.edu/~ermon/papers/imitation_nips2016_main.pdf)<br>
        Main presentation: Sally Ma, Lukas Burger<br>
        Critique: Vanessa Yan<br>
        
- **Mon. 11/18**

    18. [ViBand](http://www.gierad.com/assets/viband/viband.pdf)<br>
        Main presentation: Yichao Cheng, Annie Gao<br>
        Critique: Kayleigh Bishop<br>

    19. [Affordance++](http://plopes.org/wp-content/uploads/papers/2015-CHI-AffordanceLopes.pdf)<br>
        Main presentation: Jevin Jiang, Joseph Valdez<br>
        Critique: Emmanuel Adeniran, Cove Geary 

- **Wed. 11/20**

    20. [Generalized Speech Animation](http://www.yisongyue.com/publications/siggraph2017_speech.pdf)<br>
        Main presentation: Marynel<br>
        Critique: Jevin Jiang, Vanessa Yan <br>

    21. [Deep Appearance Models for Face Rendering](https://arxiv.org/pdf/1808.00362.pdf)<br>
        Main presentation: Nathan Tsoi, Sydney Thompson<br>
        Critique: Sean Hackett <br>







