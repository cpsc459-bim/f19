---
layout: page
title: Final Project
permalink: /final_project/
order: 4
---

The **final course project** is an opportunity for students to apply what they have learned in class to create interactive systems. All projects should have two components: *perception*, and *decision making*; although it is OK if most of the contributions of the work are on a single component.

&#9733; View example final projects from the Fall 2019 semester <a href="../projects_list/">here</a>.

## Overview

Potential projects would generally fall into two categories:

- **Applications:** An interactive system is built for a particular domain of interest (e.g., entertainment, health care, education, etc.). System integration is one of the main contributions for this type of work.

- **Algorithms:** A new algorithm (or model), or a variant of an existing algorithm, is built to tackle a specific perception or decision making task. While system integration is relevant, the main contribution is an algorithm or model.

The main **restrictions** for projects are that:

1. They are contextualized in some interactive application involving an agent that has to react to a dynamic environment. While projects may use simulations extensively for the bulk of the work, they should all demonstrate applicability (e.g., through a simple demo) to a real system.

2. It is allowed for students to work with particular hardware that they own or have access to through research labs. Student groups who do not have access to a robot or interactive device for their project, can use the [Shutter platform](https://gitlab.com/interactive-machines/shutter/shutter-ros/) for their final project. Shutter is equipped with a Real Sense camera, speakers, and a microphone array. Students can use laboratory laptops with modern GPUs for controlling the robot or a [Jetson TX2 Development Kit](https://developer.nvidia.com/embedded/buy/jetson-tx2-devkit). PlayStation 3 gamepads are also available for projects.

Students are encouraged to work on research problems for their final projects. Projects that aim to replicate results from existing papers are allowed as long as they have a clear evaluation plan, and a controlled experiments are conducted to systematically evaluate the results.

## Deliverables

The projects will have four deliverables:

1. **Project Proposal:** (due 10/25/19) The first deliverable is a project proposal (no longer than 3 pages) that describes:

    - the main goals for the project, i.e., what problem will the project focus on or help solve?;
    - the motivation for the project, i.e., why is the problem interesting or relevant?;
    - the approach to solve the problem of interest; 
    - how will results be evaluated; 
    - a description of any hardware needed to complete the project (may it be provided by course instructors or available to students elsewhere);
    - a working plan to accomplish the set goals, including what is expected to be accomplished by the milestone deadline and by the final project deadline; and
    - a plan for distributing the work among the students involved in the project, i.e., who will work on what?.
    <br><br>

    If the project will use of machine learning, then the project proposal **must** specify what dataset will be used to train and evaluate models. Due to the limited time to complete the project, we discourage big data collection efforts and, rather, suggest students use pre-existing datasets as much as possible.

2. **Project Milestone:** (due 11/11/19) The second deliverable is a report with intermediary results. The report should be between 3-4 pages and should be formatted according to the [2017 ACM Master Article Template](https://www.acm.org/publications/proceedings-template). Note that the ACM LaTeX templage is available through Overleaf [here](https://www.overleaf.com/gallery/tagged/acm-official#.WOuOk2e1taQ).

    The milestone report should include:

    - Title and a list of authors;
    - Introduction: explanation of the problem of interest, and overall plan for approaching the problem;
    - Related Work: list of prior work that helps contextualize the effort;
    - Technical Approach: description of the system or methods that you are applying to solve the problem of interest;
    - Intermediate/Preliminary Results: a statesment and evaluation of the results up to the milestone deadline. 
    - Summary of Contributions: Statement by each of the group members of the effort and results that they have contributed to the project.
    - Updated Working Plan: New working plan taking into the consideration the progress made so far and challanges (should include distribution of work among students given updated plan).
    <br><br>

3. **Project Presentation:** (due 12/02/19 and 12/04/19) 10 min project presentation highlighting the main goals of the project, approach, and results. Brief, interactive demos are encouraged for parts or the whole system that was developed!

4. **Final Report and Supplementary Material:** (due 12/11/19) Final project write-up and supplementary material (source code, video, etc.). 

	The **report** should be between 6-8 pages and be formatted according to the [2017 ACM Master Article Template](https://www.acm.org/publications/proceedings-template). The report should be structured like a paper from a conference (e.g., CHI, HRI, SIGGRAPH, CVPR, etc.). In particular, we suggest the following structure for the final report:

    - Title and a list of authors;
    - Abstract: brief description of the problem, approach, and key results. Should be no more than 300 words.
    - Introduction: explanation of the problem of interest, and overall plan for approaching the problem;
    - Related Work: list of prior work that helps contextualize the effort;
    - Method: description of the approach for solving the problem of interest;
    - Experiments: description of the experiments that were performed to test the proposed system or evaluate its components;
    - Conclussion and Future Work: summary of key results and a discussion of potential future work.
    <br><br>

	Students must create a brief **video** that documents how their system works and how people interacted with it if appropriate. The video should be created in an mp4 container with H.264 format, which is portable and easily playable. The video submission should not exceed 50MB in size. Tip: [HandBrake](https://handbrake.fr/) can be used for free to encode video.

	Students must also provide the following **supplementary material** as part of their final project submission:
	
	- A zip file including relevant materials that are not availale online, including the project source code and any trained models (e.g., checkpoints for neural networks) if you used machine learning for your project. 
	- A document describing how to run the code and any other relevant supplementary material to help understand the progress that you made towards your goals (there is no page limit for this document). This document must include a statement of contributions for each of the members of the team. Each statement must summarize the contributions that the corresponding student made to the project from the beginning of the collaboration. The document may also include a link to a website documenting results from experiments, interactive visualizations of the perception part of your system, etc.

**There are no late days for any of the final project deliverables.**

## Collaboration Policy

Students should work in teams of 3 people. 

## Honor Code
Students may consult publications and online material for ideas and code that they may want to incorporate in their project, as long as they clearly cite sources in their code and reports, as well as explain the contributions of their work. The exception is looking or incorporating another group's code into their project. 

If students are combining the course project with the project from another class, the project proposal, milestones and final report should clearly explain what portion of the project should be counted for CPSC-459/559. Separate reports should be prepared for each course.


