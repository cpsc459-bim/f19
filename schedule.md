---
layout: page
title: Schedule (F19)
permalink: /schedule/
order: 2
---


- Wed. 08/28. **Introduction.**<br>
    <u>Assignments:</u> Complete enrollment form. See Canvas for the link.
    <!--Do a test where students guess how some system works, 
like: Jhonny Lee's wii stuff -->

- Fri. 08/30. **Visual Perception: Projective Geometry & Pinhole Camera Model.**<br>
    <u>Readings:</u> [[HZ](http://www.robots.ox.ac.uk/~vgg/hzbook/)] Ch. 3.1, 3.2, 6.2.<br>
    <u>Assignments:</u> [Assignment 1](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) is published. Also, the paper request form is out - see Canvas for the link.  
    
- Mon. 09/02. **No class.** (*Labor Day*) -- We met the prior Friday.<br/>
    <u>Assignments:</u> Paper request form due.
    <!--Assign student presentations-->

- Wed. 09/04. **Visual Perception: Stereo Matching & Active Depth Sensing.**<br>
    <u>Readings:</u> [[HZ](http://www.robots.ox.ac.uk/~vgg/hzbook/)] Ch. 9, 10<br>
	<u>Assignments:</u> [Assignment 1](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) is due. [Assignment 2](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) out.
<!--Include info about Kinect pose estimation. https://courses.engr.illinois.edu/cs498dh/fa2011/lectures/Lecture%2025%20-%20How%20the%20Kinect%20Works%20-%20CP%20Fall%202011.pdf-->
<!--[Real-Time Human Pose Recognition](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/BodyPartRecognition.pdf)-->

- Mon. 09/09. **Recursive State Estimation: Kalman Filters.**<br>
    <u>Readings:</u> [[TBF](http://www.probabilistic-robotics.org/)] Ch. 2, 3 - 3.3.<br>

- Wed. 09/11. **Visual Perception: Applications.** (*student presentations*)<br>
	<u>Readings:</u> [RetroDepth](https://www.microsoft.com/en-us/research/project/retrodepth-3d-silhouette-sensing-for-high-precision-input-on-and-above-physical-surfaces/); [OmniTouch](http://www.chrisharrison.net/projects/omnitouch/omnitouch.pdf).
	
    
- Mon. 09/16. **Recursive State Estimation: Non-Parametric Filters.**<br>
    <u>Readings:</u> [[TBF](http://www.probabilistic-robotics.org/)] Ch. 4.<br>
    <u>Assignments:</u> [Assignment 2](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) due. [Assignment 3](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) out.
<!-- robot localization -->

- Wed. 09/18. **Recursive State Estimation: Applications.** (*student presentations*)<br>
    <u>Readings:</u> [AnglePose](http://ftp.dcs.glasgow.ac.uk/~rod/publications/RogWilSteMur11.pdf); [Detecting Moving Objects using a Single Camera on a Mobile Robot](http://robotics.usc.edu/publications/media/uploads/pubs/372.pdf).
    
- Mon. 09/23. **Function Approximation: Feed-Forward Neural Networks (NN).**<br>
    <u>Readings:</u> [Stanford's CS231n Notes (Module 1)](http://cs231n.github.io/); [[GBC](https://www.deeplearningbook.org/)] Ch. 11.<br>
    <!--https://www.r-bloggers.com/a-primer-on-universal-function-approximation-with-deep-learning-in-torch-and-r/-->
    <!--http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.703.5244&rep=rep1&type=pdf-->
     
- Wed. 09/25. **Function Approximation: Losses, Regularization.**<br>
    <u>Readings:</u> [Stanford's CS321n Notes (Module 2, CNNs)](http://cs231n.github.io/convolutional-networks/), [[GBC](https://www.deeplearningbook.org/)] Ch. 10-10.1.<br>
    <u>Assignments:</u> [Assignment 3](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) due. [Assignment 4](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) out.
    <!--https://www.iro.umontreal.ca/~lisa/pointeurs/TR1312.pdf-->
   
- Mon. 09/30. **Function Approximation: Convolutional NN and Applications.** (*student presentations*)<br>
    <u>Readings:</u> [YOLO](https://pjreddie.com/media/files/papers/yolo.pdf).
   
- Wed. 10/02. **Function Approximation: Recurrent NN + Applications** (*student presentation*) <br>
    <u>Readings:</u> [Multi-Person 2D Pose Estimation](https://arxiv.org/abs/1611.08050)<br>
    
    
- Mon. 10/07. **Invited Lecture**. (Brian Scassellati)  <br>
	<u>Assignments:</u> [Assignment 4](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) due. [Assignment 5](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) out. 
    <!--Value Iteration.-->
    <!--Just record: [SUGILITE](http://www.cs.cmu.edu/~NatProg/papers/p6038-li-Sugilite-CHI2017.pdf)-->
    
- Wed. 10/09. **Video Lecture**. (Ali Rahimi, "Let's take machine learning from alchemy to electricity").<br>
    <!-- Nice intro to covariance shift: https://www.analyticsvidhya.com/blog/2017/07/covariate-shift-the-hidden-problem-of-real-world-data-science/ -->

- Mon. 10/14. **Generative Adversarial NN & More Applications.**<br>
    <u>Readings:</u> [[GBC](https://www.deeplearningbook.org/)] Ch. 20.10.4; [Attention is All You Need](https://arxiv.org/abs/1706.03762)<br>
    
- Wed. 10/16. **No class.** (*October Recess*)<br>

<!--https://www.youtube.com/watch?v=ORHFOnaEzPc-->
<!--Marynel OOO-->
- Mon. 10/21. **Sequential Decision Making: Markov Decision Processes & Imitation Learning (IL).**<br>
    <u>Readings:</u> [Markov Decision Processes: Concepts and Algorithms](https://pdfs.semanticscholar.org/968b/ab782e52faf0f7957ca0f38b9e9078454afe.pdf); [An Invitation to Imitation](https://www.ri.cmu.edu/publications/an-invitation-to-imitation/).<br/>
    <u>Assignments:</u> [Assignment 5](https://gitlab.com/cpsc459-bim/assignments/f19-assignments/) due. 

- Wed. 10/23. **Sequential Decision Making: IL for Autonomous Driving and Manipulation.** (*student presentations*)<br>
    <u>Readings:</u> [Keyframe-based Learning from Demonstration](https://www.cc.gatech.edu/social-machines/papers/akgun12-soro.pdf); [Agile Autonomous Driving](https://www.cc.gatech.edu/~bboots3/files/Deep_Imitation_RSS18.pdf).<br/>
    <!--[On Offline Evaluation of Vision-based Driving Models](http://vladlen.info/papers/driving-evaluation.pdf). <br>-->
	
- Fri. 10/25. **Not a regular class day. Just an assignment due date.**<br>
	<u>Assignments:</u> [Project proposal](final_project/) due.
    
    <!--**Sequential Decision Making: IL for Robot Manipulation.** (*student presentations*)<br>
<u>Readings:</u> [Keyframe-based Learning from Demonstration](https://www.cc.gatech.edu/social-machines/papers/akgun12-soro.pdf); [Neural Task Programming](https://arxiv.org/pdf/1710.01813.pdf).<br>-->
    
- Mon. 10/28. **Sequence Prediction Revisited.** (*student presentations*)<br>
<u>Readings:</u> [Inference Machines](http://www.cs.cmu.edu/~arunvenk/papers/2016/Venkatraman_ijcai_16.pdf); [Scheduled Sampling](https://arxiv.org/abs/1506.03099).<br>
    <!--http://www.aaai.org/Papers/AAAI/1993/AAAI93-069.pdf-->
    <!-- explain texplore and talk about Monte Carlo Tree Search -->

- Wed. 10/30. **Sequential Decision Making: Reinforcement Learning (RL)**<br>
<u>Readings:</u> [[SB 2nd Ed.](https://drive.google.com/file/d/1xeUDVGWGUUv1-ccUMAZHJLej2C7aAFWY/view)] Ch. 6.
<!--todo. say something about fitted q iteration -->

   
- Mon. 11/04. **Sequential Decision Making: RL for Games** (*student presentations*)<br>
<u>Readings:</u> [DQN](https://storage.googleapis.com/deepmind-media/dqn/DQNNaturePaper.pdf); [Mastering the Game of Go](https://storage.googleapis.com/deepmind-media/alphago/AlphaGoNaturePaper.pdf).<br>

- Wed. 11/06. **Sequential Decision Making: RL in Robotics (+More on Games).** (*student presentations*)<br>
<u>Readings:</u> [How to Shape the Humor of a Robot - Social Behavior Adaptation Based on Reinforcement Learning](https://dl.acm.org/citation.cfm?id=3242976); [Trust Region Policy Optimization](https://arxiv.org/pdf/1502.05477.pdf).

<!--[Learning Hand-Eye Coordination for Robotic Grasping with Deep Learning
and Large-Scale Data Collection](https://arxiv.org/pdf/1603.02199.pdf)-->
<!-- todo. change ML reading to robot learning to be funny! change iolanda's paper too to something that shows the system being used in practice -->

- Mon. 11/11. **Sequential Decision Making: Inverse Reinforcement Learning (IRL).**<br>
<u>Readings:</u> [Apprenticeship Learning via IRL](https://ai.stanford.edu/~ang/papers/icml04-apprentice.pdf); [Max. Ent. IRL](https://www.aaai.org/Papers/AAAI/2008/AAAI08-227.pdf).<br/>
<u>Assignments:</u> Project milestone due.
<!--A few words on GAIL: [Socially Compliant Navigation](https://arxiv.org/abs/1710.02543)-->

- Wed. 11/13. **Sequential Decision Making: IRL Applications.** (*student presentations*)<br>
<u>Readings:</u> [Probabilistic Pointing Target Prediction](http://www.cs.cmu.edu/~./bziebart/publications/pointing-target-prediction.pdf); [Generative Adversarial Imitation Learning](https://cs.stanford.edu/~ermon/papers/imitation_nips2016_main.pdf).

- Mon. 11/18. **Other Applications: On-Body Interactions.** (*student presentations*)<br>
<u>Readings:</u> [ViBand](http://www.gierad.com/assets/viband/viband.pdf);
[Affordance++](http://plopes.org/wp-content/uploads/papers/2015-CHI-AffordanceLopes.pdf).
 
- Wed. 11/20. **Other Applications: Avatars.** (*student presentations*)<br>
<u>Readings:</u> [Generalized Speech Animation](http://www.yisongyue.com/publications/siggraph2017_speech.pdf); [Deep Appearance Models for Face Rendering](https://arxiv.org/pdf/1808.00362.pdf).

- Mon. 11/25. **No class** (*November Recess*)

- Wed. 11/27. **No class** (*November Recess*)

- Mon. 12/02. **Final Project Presentations.**<br>

- Wed. 12/04. **Final Project Presentations (continued).**<br>

- Mon. 12/11. **No class. Just an assignment due date.** <br>
<u>Assignments:</u> [Final project reports, videos & supplementary](final_project/) are due.
<!--Do a test where students guess how some system works, 
like: Jhonny Lee's wii stuff -->
<!--Talk about rapid fabrication. 
**Example Applications: Rapid Fabrication** (*student presentations*)<br>
<u>Readings:</u> [Printed Optics](http://www.karlddwillis.com/projects/printed-optics/); [Interactive Construction](http://groups.csail.mit.edu/hcie/files/research-projects/constructable/2012-uist-constructable-paper.pdf); [Interactiles](https://xiaoyizhang.me/assets/Paper/ASSETS_2018_Interactiles.pdf)
-->






